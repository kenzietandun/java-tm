package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JEditorPane;
import java.awt.Canvas;
import java.awt.Color;

public class Machine {

    private JFrame frmCoscHelper;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Machine window = new Machine();
                    window.frmCoscHelper.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Machine() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmCoscHelper = new JFrame();
        frmCoscHelper.setTitle("Machine");
        frmCoscHelper.setBounds(100, 100, 800, 600);
        frmCoscHelper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmCoscHelper.getContentPane().setLayout(null);
        frmCoscHelper.setLocationRelativeTo(null);
        
        DrawingCanvas dc = new DrawingCanvas();
        dc.setBackground(Color.black);
        dc.setBounds(10, 10, 778, 471);
        frmCoscHelper.getContentPane().add(dc);
        frmCoscHelper.setVisible(true);
    }
}
