package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class DrawingCanvas extends JPanel {
    public DrawingCanvas() {
        setBorder(BorderFactory.createLineBorder(Color.black));
        
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
                    drawCircle(e.getX(), e.getY());
                }
            }
        });
    }
    
    private void drawCircle(int x, int y) {
        System.out.println(x + " " + y);
    }
    
    public Dimension getPreferredSize() {
        return new Dimension(250, 200);
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawString("This is my custom panel", 10, 20);
    }
}
