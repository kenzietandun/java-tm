package turingMachine;

import java.util.ArrayList;

public class State {
    /**
     * Name of the State
     */
    private String name;
    /**
     * List of transitions a state has
     */
    private ArrayList<Transition> transitions;
    /**
     * Is it accepting state
     */
    private boolean isAcceptState;
    /**
     * Currently selected transition
     */
    private Transition selectedTransition = null;
    
    /**
     * Default constructor for State
     * @param name The name of the state
     * @param isAcceptState True if accepting state, false otherwise
     */
    public State(String name, boolean isAcceptState) {
        this.name = name;
        this.isAcceptState = isAcceptState;
        this.transitions = new ArrayList<>();
    }
    
    /**
     * Returns the name of the state
     * @return The name of the state
     */
    public String getName() {
        return name;
    }
    
    /**
     * Returns whether state is accepting or not
     * @return True if state is accepting, false otherwise
     */
    public boolean getIsAcceptState() {
        return isAcceptState;
    }
    
    /**
     * Adds a transition from this state to another state
     * @param t The transition object
     */
    public void addTransitions(Transition t) {
        if (this.isAcceptState) {
            throw new RuntimeException("Accept state does not need transitions");
        }
        for (Transition trans : transitions) {
            if (trans == t) {
                throw new RuntimeException("Transition already exists!");
            }
        }
        transitions.add(t);
    }
    
    /**
     * Returns the corresponding transition with given input
     * @param input The char input
     */
    public void getTransition(char input) {
        for (Transition trans : transitions) {
            if (trans.getInput() == input) {
                selectedTransition = trans;
                return;
            }
        }
        char cInput = input;
        if (input == ' ') {
        	cInput = '_';
        }
        throw new RuntimeException("No transition found from " + name + " with input " + cInput);
    }
    
    /**
     * Returns the output of the transition
     * @return Output of the transition
     */
    public char getTransitionOutput() {
        return selectedTransition.getOutput();
    }
    
    /**
     * Returns the head direction of the transition
     * @return Head direction of the transition
     */
    public char getTransitionMove() {
        return selectedTransition.getMove();
    }

    /**
     * Returns the next state after the transition
     * @return Next state after the transition
     */
    public String getTransitionNextState() {
        return selectedTransition.getNextState();
    }
    
    /**
     * Returns whether 2 state objects are equal by checking 
     * their name
     * @param other The other state
     * @return True if equal, false otherwise
     */
    public boolean equals(State other) {
        return this.getName() == other.getName();
    }
    
    /**
     * Returns the string representation of a state
     */
    public String toString() {
        String template = "";
        template += "State: " + getName() + "\n";
        for (Transition t : transitions) {
            template += t;
        }
        return template;
    }
}