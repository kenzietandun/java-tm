package turingMachine;

public class Transition {
    /**
     * The input string
     */
    private char input;
    /**
     * The output string
     */
    private char output;
    /**
     * Where to move the head to (L, R, N)
     */
    private char moveHead;
    /**
     * Next state after the transition
     */
    private String nextState;
    
    /**
     * Default constructor for Transition
     * @param input Input from the tape
     * @param output Output to the tape
     * @param move Direction to move the head
     * @param nextState The next state after transition
     */
    public Transition(char input, char output, char move, String nextState) {
        this.input = input;
        if (input == '_') {
        	this.input = ' ';
        }
        this.output = output;
        if (output == '_') {
        	this.output = ' ';
        }
        if (move == 'L' || move == 'R' || move == 'N') {
            moveHead = move;
        } else {
            throw new RuntimeException("Not a valid move head instruction");
        }
        this.nextState = nextState;
    }
    
    /**
     * Returns the input
     * @return Input
     */
    public char getInput() {
        return input;
    }
    
    /**
     * Returns the output
     * @return Output
     */
    public char getOutput() {
        return output;
    }
    
    /**
     * Returns the move direction
     * @return Move direction
     */
    public char getMove() {
        return moveHead;
    }
    
    /**
     * Returns the next state
     * @return Next state after transition
     */
    public String getNextState() {
        return nextState;
    }
    
    /**
     * Check if 2 transitions are equal
     * @param other The other transition
     * @return True if equal, false otherwise
     */
    public boolean equals(Transition other) {
        return this.input == other.getInput();
    }
    
    /**
     * Returns a string representation of a Transition obj
     */
    public String toString() {
        char char_input = input;
        char char_output = output;

        if (input == ' ') {
            char_input = '_';
        }

        if (output == ' ') {
            char_output = '_';
        }
        String template = "  " + char_input + "/" + 
                char_output + "," + moveHead + " --> " + nextState + "\n";
        return template;
    }
}
