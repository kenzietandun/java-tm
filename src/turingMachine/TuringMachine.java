package turingMachine;

import java.util.ArrayList;
// Use JSON Simple to write save files
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

// JSON Simple to read JSON files
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;

/** 
 * A simple Turing Machine implementation
 * @author Kenzie Tandun
 */
public class TuringMachine {
    private State currentState;
    private ArrayList<State> states;
    private ArrayList<Character> tape;
    private int currentTapeLocation;
    private boolean hasFinished;
    
    /**
     * Default constructor for TuringMachine
     * It takes an ArrayList of states as a parameter
     * and constructs a simple Turing machine
     * @param states
     */
    public TuringMachine(ArrayList<State> states) {
        this.currentState = states.get(0);
        this.states = states;
        this.tape = new ArrayList<>();
        this.currentTapeLocation = 0;
        this.hasFinished = false;
        
        boolean hasDuplicate = checkStatesDuplicates();
        if (hasDuplicate) {
            throw new RuntimeException("There are duplicate states");
        }
    }
    
    /**
     * Checks for duplicate states in the TuringMachine
     * @return True if has duplicate states, false otherwise
     */
    public boolean checkStatesDuplicates() {
        for (State s : states) {
            if (Collections.frequency(states, s) > 1) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Adds a string as an input for the machine
     * @param input The input string
     */
    public void addInputString(String input) {
        for (int i = 0; i < input.length(); i++) {
            tape.add(input.charAt(i));
        }
    }
    
    /**
     * Checks whether the machine has reached an accepting state
     * @return True if has reached accepting state, false otherwise
     */
    public boolean getHasFinished() {
        return hasFinished;
    }
    
    /**
     * Returns a string representation of the current state
     * @return String representation of the current state
     */
    public String getCurrentState() {
        return currentState.getName();
    }
    
    /**
     * Returns a current representation of the current tape
     * and location of the head
     * @return Current representation of the tape and head
     */
    public String getTape() {
        String result = "";
        if (currentTapeLocation < 0)
            result += "_";

        for (char c : tape) {
            result += c;
        }
        result += "\n";
        
        for (int i = 0; i < currentTapeLocation; i++) {
            result += " ";
        }
        result += "^";
        
        return result;
    }

    /**
     * Transitions from current state to the next state according
     * to the input
     */
    public void transition() {
        char currentChar = ' ';
        try {
            currentChar = tape.get(currentTapeLocation);
        } catch (IndexOutOfBoundsException err) {
            // do nothing
        }
        currentState.getTransition(currentChar);
        char output = currentState.getTransitionOutput();
        char move = currentState.getTransitionMove();
        writeToTape(output, move);

        String nextState = currentState.getTransitionNextState();
        for (State s : states) {
            if (s.getName() == nextState) {
                currentState = s;
                break;
            }
        }
        if (currentState.getIsAcceptState()) {
            hasFinished = true;
        }
        
    }
    
    /**
     * Writes the output of the transition to the tape
     * and move the head accordingly
     * @param c The letter to write to the tape
     * @param move Direction to move the head to
     */
    public void writeToTape(char c, char move) {
        // Writes to the tape at the specified location
        if (currentTapeLocation == tape.size()) { // end of string
            tape.add(c);
        } else if (currentTapeLocation < 0) { // beginning of string
            tape.add(0, c);
            currentTapeLocation = 0;
        } else { // middle of string
            tape.set(currentTapeLocation, c);
        }

        // Move the head after writing
        switch(move) {
        case 'L':
            currentTapeLocation--;
            break;
        case 'R':
            currentTapeLocation++;
            break;
        default:
            break;
        }
    }
    
    /**
     * Reads states configuration from Quiz server and parse them into 
     * States object that is understandable by the Turing Machine
     * @param configuration JSON configuration
     * @return ArrayList of State 
     * @throws FileNotFoundException 
     */
    public static ArrayList<State> readStatesJSON(String configFile) throws FileNotFoundException {
    	File saveFile = new File(configFile);
    	if (!saveFile.exists()) {
    		throw new FileNotFoundException("No saved file found");
    	}
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(configFile));
            JSONObject savedState = (JSONObject) obj;
            JSONArray nodesJSON = (JSONArray) savedState.get("nodes");
            JSONArray edgesJSON = (JSONArray) savedState.get("edges");
            
            ArrayList<State> foundStates = new ArrayList<>();
            for (Object node : nodesJSON) {
            	JSONArray nodeDesc = (JSONArray) node;
            	String nodeName = (String) nodeDesc.get(0);
            	boolean nodeIsAccepting = (boolean) nodeDesc.get(1);
            	foundStates.add(new State(nodeName, nodeIsAccepting));
            }

            for (Object edge : edgesJSON) {
            	JSONArray edgeDesc = (JSONArray) edge;
            	int nodeSrc = (int) (long) edgeDesc.get(0);
            	int nodeDest = (int) (long) edgeDesc.get(1);

            	if (nodeSrc == -1) { 
            		// found the starting point, set it to the
            		// beginning of the State ArrayList
            		State temp = foundStates.get(nodeDest);
            		foundStates.set(nodeDest, foundStates.get(0));
            		foundStates.set(0, temp);
            		continue;
            	}
            	String transition = (String) edgeDesc.get(2);
            	char input = transition.split("/")[0].charAt(0);
            	char output = transition.split("/")[1].split(",")[0].charAt(0);
            	char move = transition.split(",")[1].charAt(0);
            	String nextState = foundStates.get(nodeDest).getName();
            	foundStates.get(nodeSrc).addTransitions((new Transition(input, output, move, nextState)));
            }
            
            return foundStates;

        } catch (FileNotFoundException err) {
            err.printStackTrace();
        } catch (IOException err) {
            err.printStackTrace();
        } catch (ParseException err) {
            err.printStackTrace();
        }
        
        return null;
    }
    
    public static void main(String[] args) {
        // Test using number 8 from Quiz 10
		ArrayList<State> test = null;
    	try {
			test = TuringMachine.readStatesJSON("src/turingMachine/test.json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        TuringMachine tm = new TuringMachine(test);
        tm.addInputString("11111111110000010000211111111110000000000");
        
        while (!tm.getHasFinished()) {
			System.out.println(tm.getCurrentState());
            System.out.println(tm.getTape());
            try {
				tm.transition();
            } catch (RuntimeException e) {
            	System.out.println("Not accepted");
            	System.out.println(e.getMessage());
            	return;
            }
        }
        System.out.println(tm.getTape());
        System.out.println("String is accepted successfully");
    }
}